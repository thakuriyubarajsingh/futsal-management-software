﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class BookingTable
    {
        public int bookingId { get; set; }
        public DateTime Date { get; set; }
        public string bookingtype { get; set; }
        public string paidoption { get; set; }

    }
}