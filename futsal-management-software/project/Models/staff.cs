﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class staff
    {
        public int staffid { get; set; }
        public string staffname { get; set; }
        public string staffemail { get; set; }
        public int staffcontact { get; set; }
    }
}