﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project.Models
{
    public class BlockedUser
    {
        public int U_ID { get; set; }

        [Required]
        public int Strike { get; set; }

        [Required]
        public string Discription { get; set; }
    }
}