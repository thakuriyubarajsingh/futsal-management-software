﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project.Models
{
    public class Feedback
    {
        [Key]
        public int F_ID { get; set; }

        [Required]
        public string Feedback_text { get; set; }

        public int U_ID { get; set; }
    }
}