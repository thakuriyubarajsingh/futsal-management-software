﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class Booking
    {
        [Key]
        public int Booking_ID { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [Required]
        public int Price { get; set; }

        public int Duration { get; set; }
    }
}