﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class User
    {
        [Key]
        public int U_ID { get; set; }

        public int M_ID { get; set; }

        public int P_ID { get; set; }
    }
}