﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project.Models
{
    public class FutshalPrice
    {
        [Required(ErrorMessage = "This field cannot be empty")]
        public DateTime Time { get; set; }

        [Required(ErrorMessage = "This fileld cannot be Empty.")]
        public int Price { get; set; }
        
        public bool ISweekend { get; set; }
    }
}