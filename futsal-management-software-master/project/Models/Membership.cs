﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project.Models
{
    public class Membership
    {
        public int M_ID { get; set; }
        public int Package_ID { get; set; }
        public int U_ID { get; set; }
    }
}