﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace project.Models
{
    public class Person
    {
        [Key]
        public int P_ID { get; set; }

        [Required(ErrorMessage = "First Name cannot be empty")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Last Name cannot be empty")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "Email cannot be empty")]
        [EmailAddress(ErrorMessage = "Enter a valid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password cannot be empty")]
        [DataType(DataType.Password)]
        [StringLength(20,ErrorMessage = "Enter upto 20 character")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Phone No cannot be empty")]
        [Display(Name = "Phone Number")]
        public Int64 ContactNo { get; set; }

        [Required(ErrorMessage = "Gender cannot be empty")]
        public string Gender { get; set; }
    }
}