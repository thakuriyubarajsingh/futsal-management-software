﻿using project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SignIn()
        {
            ViewBag.Message = "Your SignIn page.";

            return View();
        }

        /*[HttpPost]
        public ActionResult SignIn(user U)
        {
            //System.Diagnostics.Debug.WriteLine(U.Username);
            return View();
        }*/
    }

    
}