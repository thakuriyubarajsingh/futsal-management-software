﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using project.Models;

namespace project.DAL
{
    public class projectContext : DbContext
    {
        public projectContext()
        {

        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Booking> Bookings { get; set; }
    }
}